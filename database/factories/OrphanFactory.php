<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Orphan;
use Faker\Generator as Faker;

$factory->define(Orphan::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'firstname' => $faker->word,
        'lastname' => $faker->word,
        'dateofbirth' => $faker->date('Y-m-d H:i:s'),
        'placeofbirth' => $faker->word,
        'orphanage_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
