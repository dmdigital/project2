<div class="table-responsive-sm">
    <table class="table table-striped" id="orphanages-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Address</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($orphanages as $orphanage)
            <tr>
                <td>{{ $orphanage->name }}</td>
            <td>{{ $orphanage->address }}</td>
                <td>
                    {!! Form::open(['route' => ['orphanages.destroy', $orphanage->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('orphanages.show', [$orphanage->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('orphanages.edit', [$orphanage->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>