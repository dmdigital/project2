<div class="table-responsive-sm">
    <table class="table table-striped" id="orphans-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Dateofbirth</th>
        <th>Placeofbirth</th>
        <th>Orphanage </th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($orphans as $orphan)
            <tr>
                <td>{{ $orphan->name }}</td>
            <td>{{ $orphan->firstname }}</td>
            <td>{{ $orphan->lastname }}</td>
            <td>{{ $orphan->dateofbirth }}</td>
            <td>{{ $orphan->placeofbirth }}</td>
            <td>{{ $orphan->orphanage->name }}</td>
                <td>
                    {!! Form::open(['route' => ['orphans.destroy', $orphan->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('orphans.show', [$orphan->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('orphans.edit', [$orphan->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>