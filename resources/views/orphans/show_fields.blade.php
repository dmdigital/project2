<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $orphan->name }}</p>
</div>

<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstname', 'Firstname:') !!}
    <p>{{ $orphan->firstname }}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastname', 'Lastname:') !!}
    <p>{{ $orphan->lastname }}</p>
</div>

<!-- Dateofbirth Field -->
<div class="form-group">
    {!! Form::label('dateofbirth', 'Dateofbirth:') !!}
    <p>{{ $orphan->dateofbirth }}</p>
</div>

<!-- Placeofbirth Field -->
<div class="form-group">
    {!! Form::label('placeofbirth', 'Placeofbirth:') !!}
    <p>{{ $orphan->placeofbirth }}</p>
</div>

<!-- Orphanage Id Field -->
<div class="form-group">
    {!! Form::label('orphanage_id', 'Orphanage Id:') !!}
    <p>{{ $orphan->orphanage->name }}</p>
</div>

