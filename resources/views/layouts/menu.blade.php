
<li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('users.index') !!}">
        <i class="cil-user"></i>
        <span>Users</span>
    </a>
</li>

<li class="nav-item {{ Request::is('orphanages*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orphanages.index') }}">
        <i class="cil-library-building"></i>
        <span>Orphanages</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orphans*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orphans.index') }}">
        <i class="fa fa-child"></i>
        <span>Orphans</span>
    </a>
</li>
