<?php

namespace App\Repositories;

use App\Models\Orphanage;
use App\Repositories\BaseRepository;

/**
 * Class OrphanageRepository
 * @package App\Repositories
 * @version October 7, 2020, 5:02 am UTC
*/

class OrphanageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'address'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Orphanage::class;
    }
}
