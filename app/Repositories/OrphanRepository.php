<?php

namespace App\Repositories;

use App\Models\Orphan;
use App\Repositories\BaseRepository;

/**
 * Class OrphanRepository
 * @package App\Repositories
 * @version October 7, 2020, 10:01 am UTC
*/

class OrphanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'firstname',
        'lastname',
        'dateofbirth',
        'placeofbirth',
        'orphanage_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Orphan::class;
    }
}
