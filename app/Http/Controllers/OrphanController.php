<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrphanRequest;
use App\Http\Requests\UpdateOrphanRequest;
use App\Repositories\OrphanRepository;
use App\Repositories\OrphanageRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OrphanController extends AppBaseController
{
    /** @var  OrphanRepository */
    private $orphanRepository;
    private $orphanageRepository;

    public function __construct(OrphanRepository $orphanRepo, OrphanageRepository $orphanageRepo)
    {
        $this->orphanRepository = $orphanRepo;
        $this->orphanageRepository = $orphanageRepo;
    }

    /**
     * Display a listing of the Orphan.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orphans = $this->orphanRepository->all();

        return view('orphans.index')
            ->with('orphans', $orphans);
    }

    /**
     * Show the form for creating a new Orphan.
     *
     * @return Response
     */
    public function create()
    {
        $orphanages = $this->orphanageRepository->all();
        
        $orphan = $this->orphanRepository->model();        
        $orphan = new $orphan; 

        $orphanages = $this->getdropdownData($orphanages);

        return view('orphans.create')
            ->with('orphan', $orphan)
            ->with('orphanages', $orphanages);

    }

    /**
     * Store a newly created Orphan in storage.
     *
     * @param CreateOrphanRequest $request
     *
     * @return Response
     */
    public function store(CreateOrphanRequest $request)
    {
        $input = $request->all();

        $orphan = $this->orphanRepository->create($input);

        Flash::success('Orphan saved successfully.');

        return redirect(route('orphans.index'));
    }

    /**
     * Display the specified Orphan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orphan = $this->orphanRepository->find($id);

        if (empty($orphan)) {
            Flash::error('Orphan not found');

            return redirect(route('orphans.index'));
        }

        return view('orphans.show')->with('orphan', $orphan);
    }

    /**
     * Show the form for editing the specified Orphan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orphan = $this->orphanRepository->find($id);

        if (empty($orphan)) {
            Flash::error('Orphan not found');

            return redirect(route('orphans.index'));
        }

        $orphanages = $this->orphanageRepository->all();

        $orphanages = $this->getdropdownData($orphanages);

        return view('orphans.edit')
            ->with('orphan', $orphan)
            ->with('orphanages', $orphanages);
    }

    /**
     * Update the specified Orphan in storage.
     *
     * @param int $id
     * @param UpdateOrphanRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrphanRequest $request)
    {
        $orphan = $this->orphanRepository->find($id);

        if (empty($orphan)) {
            Flash::error('Orphan not found');

            return redirect(route('orphans.index'));
        }

        $orphan = $this->orphanRepository->update($request->all(), $id);

        Flash::success('Orphan updated successfully.');

        return redirect(route('orphans.index'));
    }

    /**
     * Remove the specified Orphan from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orphan = $this->orphanRepository->find($id);

        if (empty($orphan)) {
            Flash::error('Orphan not found');

            return redirect(route('orphans.index'));
        }

        $this->orphanRepository->delete($id);

        Flash::success('Orphan deleted successfully.');

        return redirect(route('orphans.index'));
    }
}
