<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrphanageRequest;
use App\Http\Requests\UpdateOrphanageRequest;
use App\Repositories\OrphanageRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OrphanageController extends AppBaseController
{
    /** @var  OrphanageRepository */
    private $orphanageRepository;

    public function __construct(OrphanageRepository $orphanageRepo)
    {
        $this->orphanageRepository = $orphanageRepo;
    }

    /**
     * Display a listing of the Orphanage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orphanages = $this->orphanageRepository->all();

        return view('orphanages.index')
            ->with('orphanages', $orphanages);
    }

    /**
     * Show the form for creating a new Orphanage.
     *
     * @return Response
     */
    public function create()
    {
        return view('orphanages.create');
    }

    /**
     * Store a newly created Orphanage in storage.
     *
     * @param CreateOrphanageRequest $request
     *
     * @return Response
     */
    public function store(CreateOrphanageRequest $request)
    {
        $input = $request->all();

        $orphanage = $this->orphanageRepository->create($input);

        Flash::success('Orphanage saved successfully.');

        return redirect(route('orphanages.index'));
    }

    /**
     * Display the specified Orphanage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orphanage = $this->orphanageRepository->find($id);

        if (empty($orphanage)) {
            Flash::error('Orphanage not found');

            return redirect(route('orphanages.index'));
        }

        return view('orphanages.show')->with('orphanage', $orphanage);
    }

    /**
     * Show the form for editing the specified Orphanage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orphanage = $this->orphanageRepository->find($id);

        if (empty($orphanage)) {
            Flash::error('Orphanage not found');

            return redirect(route('orphanages.index'));
        }

        return view('orphanages.edit')->with('orphanage', $orphanage);
    }

    /**
     * Update the specified Orphanage in storage.
     *
     * @param int $id
     * @param UpdateOrphanageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrphanageRequest $request)
    {
        $orphanage = $this->orphanageRepository->find($id);

        if (empty($orphanage)) {
            Flash::error('Orphanage not found');

            return redirect(route('orphanages.index'));
        }

        $orphanage = $this->orphanageRepository->update($request->all(), $id);

        Flash::success('Orphanage updated successfully.');

        return redirect(route('orphanages.index'));
    }

    /**
     * Remove the specified Orphanage from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orphanage = $this->orphanageRepository->find($id);

        if (empty($orphanage)) {
            Flash::error('Orphanage not found');

            return redirect(route('orphanages.index'));
        }

        $this->orphanageRepository->delete($id);

        Flash::success('Orphanage deleted successfully.');

        return redirect(route('orphanages.index'));
    }
}
