<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Orphanage
 * @package App\Models
 * @version October 7, 2020, 5:02 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $orphans
 * @property string $name
 * @property string $address
 */
class Orphanage extends Model
{
    use SoftDeletes;

    public $table = 'orphanages';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'address'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'address' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:255',
        'address' => 'required|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function orphans()
    {
        return $this->hasMany(\App\Models\Orphan::class, 'orphanage_id');
    }
}
