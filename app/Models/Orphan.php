<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Orphan
 * @package App\Models
 * @version October 7, 2020, 10:01 am UTC
 *
 * @property \App\Models\Orphanage $orphanage
 * @property string $name
 * @property string $firstname
 * @property string $lastname
 * @property string|\Carbon\Carbon $dateofbirth
 * @property string $placeofbirth
 * @property integer $orphanage_id
 */
class Orphan extends Model
{
    use SoftDeletes;

    public $table = 'orphans';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'firstname',
        'lastname',
        'dateofbirth',
        'placeofbirth',
        'orphanage_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'firstname' => 'string',
        'lastname' => 'string',
        'dateofbirth' => 'datetime',
        'placeofbirth' => 'string',
        'orphanage_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:255',
        'firstname' => 'required|string|max:255',
        'lastname' => 'required|string|max:255',
        'dateofbirth' => 'required',
        'placeofbirth' => 'required|string|max:255',
        'orphanage_id' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function orphanage()
    {
        return $this->belongsTo(\App\Models\Orphanage::class, 'orphanage_id');
    }
}
